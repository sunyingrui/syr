---
layout: page  
title: 视觉传达小常识
excerpt_separator: "<!--more-->"  
categories:
     - 平面设计 
---
 
 > “对视觉通过人自身进行调节达到某种程度的行为”


<!--more-->
设计一词来源于英文＂design＂，包括很广的设计范围和门类建筑：工业、环艺、装潢、展示、服装、平面设计等等，而平面设计现在的名称在平常的表述中却很为难，因为现在学科之间的交*更广更深，传统的定义，例如现行的叫法“平面设计(graphis design)视觉传达设计、装潢设计.，这也许与平面设计的特点有很大的关系，因为设计无所不在、平面设计无所不在，从范围来讲用来印刷的都和平面设计有关，从功能来讲“对视觉通过人自身进行调节达到某种程度的行为”，称之为视觉传达，即用视觉语言进行传递信息和表达观点，而装潢设计或装潢艺术设计则被公认为极不准确的名称，带有片面性。
现代设计师必须是具有宽广的文化视角深邃的智慧和丰富的知识；必须是具有创新精神知识渊博、敏感并能解决问题的人，应考虑社会反映、社会效果，力求设计作品对社会有益，能提高人们的审美能力，心理上的愉悦和满足，应概括当代的时代特征，反映了真正的审美情趣和审美理想。起码你应出阴白，优秀的设计师有他们“自己”的手法、清晰的形象、合平逻辑的观点，设计的提高必修在不断的学习和实践中进行，设计师的广泛涉猎和专注是相互矛盾又统一的，前者是灵感和表现方式的源泉，后者是工作的态度。好的设计并不只是图形的创作，他是中和了许多智力劳动的结果，涉猎不同的领城，担当不同的角色，可以让我们保持开阔的视野，可以让我们的设计带有更多的信息。
