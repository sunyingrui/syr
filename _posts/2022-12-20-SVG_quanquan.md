---
title: 如果你愿意一层一层一层的——
excerpt_separator: "<!--more-->"
categories:
  - SVG制作
tags:
  - SVG制作
  
---
 > 圈住我的心 
 



<!--more-->
---
# 鼠标触碰就可以看到图形周边的旋转了！
<style>
circle{
     transition: all 2s;
     stroke-dasharray:300,300;
     stroke-dashoffset:300;
 }
svg:hover #circle{
    stroke-dashoffset:0;
 }
</style>



<svg  width="500" height="600" viewBox="0 0 200 200">
   <circle id="circle" cx="100" cy="50" r="40" stroke="#ff87c5" stroke-width="7" fill="#00f8cc"   />
</svg>

源代码： 
```
<style>
circle{
     transition: all 2s;
     stroke-dasharray:300,300;
     stroke-dashoffset:300;
 }
svg:hover #circle{
    stroke-dashoffset:0;
 }
</style>
<svg  width="500" height="600" viewBox="0 0 200 200">
   <circle id="circle" cx="100" cy="50" r="40" stroke="#ff87c5" stroke-width="7" fill="#00f8cc"   />
</svg>
```
